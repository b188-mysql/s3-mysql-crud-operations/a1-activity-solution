-- [SECTION] INSERT
	-- users table
	INSERT INTO users (email, password, datetime_created) VALUES (
		"johnsmith@gmail.com",
		"passwordA",
		"2021-1-1 1:00:00"
	);

	INSERT INTO users (email, password, datetime_created) VALUES (
		"juandelacruz@gmail.com",
		"passwordB",
		"2021-1-1 2:00:00"
	);

	INSERT INTO users (email, password, datetime_created) VALUES (
		"janesmith@gmail.com",
		"passwordC",
		"2021-1-1 3:00:00"
	);

	INSERT INTO users (email, password, datetime_created) VALUES (
		"mariadelacruz@gmail.com",
		"passwordD",
		"2021-1-1 4:00:00"
	);

	INSERT INTO users (email, password, datetime_created) VALUES (
		"johndoe@gmail.com",
		"passwordE",
		"2021-1-1 5:00:00"
	);

	-- posts table
	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
		"First Code",
		"Hello World!",
		"2021-1-2 1:00:00",
		1
	);

	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
		"Second Code",
		"Hello Earth!",
		"2021-1-2 2:00:00",
		1
	);

	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
		"Third Code",
		"Welcome to Mars!",
		"2021-1-2 3:00:00",
		2
	);

	INSERT INTO posts (title, content, datetime_posted, author_id) VALUES (
		"Fourth Code",
		"Bye bye solar system!",
		"2021-1-2 4:00:00",
		4
	);

-- [SECTION] Retrieve
	SELECT * FROM posts WHERE author_id = 1;

	SELECT email, datetime_created FROM users;

-- [SECTION] Update
	UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- [SECTION] Delete
	DELETE FROM users WHERE email = "johndoe@gmail.com";







